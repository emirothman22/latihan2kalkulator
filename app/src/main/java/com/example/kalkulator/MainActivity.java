package com.example.kalkulator;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity implements View.OnClickListener {

    EditText vTxtA,vtxtB,vtxtC;

    protected float yTxtA,yTxtB,yTxtC;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        vTxtA = (EditText) findViewById(R.id.txtA);
        vtxtB = (EditText) findViewById(R.id.txtB);
        vtxtC = (EditText) findViewById(R.id.txtC);

        Button vBtnTambah = (Button) findViewById(R.id.btn_tambah);
        Button vBtnKurang = (Button) findViewById(R.id.btn_kurang);
        Button vBtnKali = (Button) findViewById(R.id.btn_kali);
        Button vBtnBagi = (Button) findViewById(R.id.btn_bagi);

        vBtnTambah.setOnClickListener(this);
        vBtnKurang.setOnClickListener(this);
        vBtnKali.setOnClickListener(this);
        vBtnBagi.setOnClickListener(this);

    }
    @Override
    public void onClick(View v){
        yTxtA =Float.parseFloat(vTxtA.getText().toString());
        yTxtB =Float.parseFloat(vtxtB.getText().toString());

        switch (v.getId()){
            case
            R.id.btn_tambah:
                yTxtC = yTxtA + yTxtB;

                vtxtC.setText(Float.toString(yTxtC));
                break;

            case
                    R.id.btn_kurang:
                yTxtC = yTxtA - yTxtB;

                vtxtC.setText(Float.toString(yTxtC));
                break;

            case
                    R.id.btn_kali:
                yTxtC = yTxtA * yTxtB;

                vtxtC.setText(Float.toString(yTxtC));
                break;


            case
                    R.id.btn_bagi:
                yTxtC = yTxtA / yTxtB;

                vtxtC.setText(Float.toString(yTxtC));
                break;
        }

    }
}
